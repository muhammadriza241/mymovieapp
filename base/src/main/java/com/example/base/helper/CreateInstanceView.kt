package com.example.base.helper

import android.app.Activity
import android.util.Log
import androidx.fragment.app.Fragment

object CreateInstanceView {
    fun instantiateFragment(className: String) : Fragment? {
        return try {
            Class.forName(className).newInstance() as Fragment
        } catch (e: Exception) {
            // not install feature module
            Log.d("tes", e.message.toString())
            null
        }
    }
    fun instantiateActivity(className: String) : Activity? {
        return try {
            Class.forName(className).newInstance() as Activity
        } catch (e: Exception) {
            // not install feature module
            Log.d("tesError", e.message.toString())
            null
        }
    }
}