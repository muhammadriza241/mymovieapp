package com.example.dashboard.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.dashboard.R
import com.example.dashboard.databinding.ActivityHomeBinding
import com.example.dashboard.home.fragment.GenreFragment
import com.example.dashboard.home.fragment.HomeFragment
import com.example.dashboard.module.dashboardModule
import com.example.dashboard.viewmodel.DashboardViewmodel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules

class HomeActivity : AppCompatActivity() {

    val binding by lazy { ActivityHomeBinding.inflate(layoutInflater) }

    private val viewModel: DashboardViewmodel by viewModel()


    var homeFragment = HomeFragment()
    var genreFragment = GenreFragment()
    var active: Fragment = homeFragment

    var ft: FragmentTransaction = supportFragmentManager.beginTransaction()
    val apiKey = "a5956c69becb1fc204b4a2c79a865824"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        loadKoinModules(dashboardModule)

        initListener()
        binding.lytFilter.visibility = View.INVISIBLE
    }

    private fun initListener() {
        binding.lytBottomHome.setOnClickListener {
            supportFragmentManager.beginTransaction().hide(active).show(homeFragment)
                .commit()
            active = homeFragment
            navigateWithoutAnim(0)
        }
        binding.lytBottomGenre.setOnClickListener {
            supportFragmentManager.beginTransaction().hide(active).show(genreFragment)
                .commit()
            active = genreFragment
            navigateWithoutAnim(1)
        }
    }

    override fun onResume() {
        super.onResume()
        if (!genreFragment.isAdded){
            ft.add(R.id.fragment_container,genreFragment,"2").hide(genreFragment)
        }
        if (!homeFragment.isAdded){
            ft.add(R.id.fragment_container,homeFragment,"1").commit()
        }
    }



    fun navigateWithoutAnim(index: Int) {
        //val controller = Navigation.findNavController(this, R.id.fragment_container)
        when (index) {
            0 -> {
                binding.lytFilter.visibility = View.INVISIBLE
                binding.txtPageTitle.text = resources.getString(com.example.base.R.string.home)
            }
            1 -> {
                binding.lytFilter.visibility = View.VISIBLE
                binding.txtPageTitle.text = resources.getString(com.example.base.R.string.genre)
            }
        }
    }
}