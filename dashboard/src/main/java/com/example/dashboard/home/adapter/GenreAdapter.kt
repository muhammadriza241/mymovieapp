package com.example.dashboard.home.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.core.domain.dashboard.model.GenreListEntity
import com.example.dashboard.databinding.ItemListGenreBinding

class GenreAdapter: RecyclerView.Adapter<GenreAdapter.ViewHolder>() {


    val listChoiceGenre:ArrayList<GenreListEntity?> = ArrayList()
    var listData:ArrayList<GenreListEntity?> = ArrayList()

    fun setData(data:List<GenreListEntity?>){
        listData.clear()
        listData.addAll(data)
        notifyDataSetChanged()
    }

    fun getListChoice():ArrayList<GenreListEntity?>{
        return listChoiceGenre
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreAdapter.ViewHolder {
        return ViewHolder(ItemListGenreBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: GenreAdapter.ViewHolder, position: Int) {
        holder.bind(listData[position])
    }

    override fun getItemCount(): Int  = listData.size

    inner class ViewHolder(private val binding: ItemListGenreBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(data: GenreListEntity?){
            binding.run {
                txtGenre.text = data?.name
                if (data in listChoiceGenre){
                    txtGenre.setBackgroundColor(Color.parseColor("#ffffff"));
                    txtGenre.setTextColor(Color.parseColor("#000000"))
                }else{
                    txtGenre.setBackgroundColor(Color.parseColor("#000000"));
                    txtGenre.setTextColor(Color.parseColor("#ffffff"))
                }

                txtGenre.setOnClickListener {
                    if (data in listChoiceGenre){
                        listChoiceGenre.remove(data)
                    }else{
                        listChoiceGenre.add(data)
                    }
                    notifyDataSetChanged()
                }
            }
        }
    }

}
