package com.example.dashboard.module

import com.example.dashboard.viewmodel.DashboardViewmodel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dashboardModule = module {
    viewModel { DashboardViewmodel(get()) }
}