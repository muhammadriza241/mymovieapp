package com.example.dashboard.util

interface ItemClickListener<T> {
    fun onClick(data : T)
}