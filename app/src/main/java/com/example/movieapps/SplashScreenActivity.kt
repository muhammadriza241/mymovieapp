package com.example.movieapps

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.example.base.helper.CreateInstanceView.instantiateActivity
import maes.tech.intentanim.CustomIntent

class SplashScreenActivity : AppCompatActivity() {

    companion object{
        val BASE_PACKAGE = "com.example"
    }


    val classNameHome :String
        get() = "com.example.home.dashboard.HomeActivity"

    var homeActivity: Activity? = instantiateActivity(classNameHome)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)


        val mainLooperHandler = Handler(Looper.getMainLooper())

        mainLooperHandler.postDelayed({
            try {
                val intent = Intent().setClassName(this, classNameHome)
                startActivity(intent)
                finish()
            }catch (e:Exception){
                Toast.makeText(this, "Error asu "+e.message, Toast.LENGTH_SHORT).show()
                Log.d("ErrorAssu",e.message.toString())
            }

        }, 2000)
    }
}