package com.example.movieapps

import android.app.Application
import androidx.paging.ExperimentalPagingApi
import com.example.core.di.coreModule
import com.example.core.di.repositoryModule
import com.example.core.di.usecaseModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

@ExperimentalPagingApi
open class AppController: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@AppController)
            koin.loadModules(listOf(
                coreModule,
                repositoryModule,
                usecaseModule
            ))
        }
    }

    private external fun baseUrl() : String
    private external fun apikey() : String

    fun baseURL() : String = baseUrl()
    fun apiKey() : String = apikey()
}