#include <jni.h>
#include <string>


extern "C" JNIEXPORT jstring JNICALL
Java_com_example_movieapps_AppController_baseUrl(JNIEnv *env,jobject /* this */){
    std::string baseUrl = "https://api.themoviedb.org/";
    return env->NewStringUTF(baseUrl.c_str());
}


extern "C" JNIEXPORT jstring JNICALL
Java_com_example_movieapps_AppController_apikey(JNIEnv *env,jobject /* this */){
    std::string apikey = "a5956c69becb1fc204b4a2c79a865824";
    return env->NewStringUTF(apikey.c_str());
}