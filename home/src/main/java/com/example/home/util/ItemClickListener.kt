package com.example.home.util
interface ItemClickListener<T> {
    fun onClick(data: T)
}