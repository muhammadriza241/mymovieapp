package com.example.home.dashboard.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.core.data.dashboard.response.ResultsItem
import com.example.home.databinding.ItemMovieListBinding
import com.example.home.detail.DetailActivity
import com.example.home.util.ItemClickListener
import com.example.home.viewmodel.HomeViewModel

class MovieListPagingAdapter:
    PagingDataAdapter<HomeViewModel.UiMovieModel.ListMovie, RecyclerView.ViewHolder>(
        UIMODEL_COMPARATOR
    ){

    var itemWithMargin = 1

    var listener: ItemClickListener<ResultsItem?>? = null

    fun setItemCallback(itemClickListener: ItemClickListener<ResultsItem?>?){
        listener = itemClickListener
    }

    companion object{
        private val UIMODEL_COMPARATOR = object : DiffUtil.ItemCallback<HomeViewModel.UiMovieModel.ListMovie>(){
            override fun areItemsTheSame(oldItem: HomeViewModel.UiMovieModel.ListMovie, newItem: HomeViewModel.UiMovieModel.ListMovie): Boolean {
                return (oldItem is HomeViewModel.UiMovieModel.ListMovie && newItem is HomeViewModel.UiMovieModel.ListMovie && oldItem.dataListMovie.id == newItem.dataListMovie.id && oldItem.dataListMovie.id == newItem.dataListMovie.id)
            }

            override fun areContentsTheSame(oldItem: HomeViewModel.UiMovieModel.ListMovie, newItem: HomeViewModel.UiMovieModel.ListMovie): Boolean = oldItem==newItem
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val uiModel = getItem(position)
        (holder as ViewHolder).bind(uiModel?.dataListMovie!!)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(ItemMovieListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    inner class ViewHolder(private val binding: ItemMovieListBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResultsItem) {
            binding.run {
                txtTitle.text = data.title
                Glide.with(binding.root)
                    .load("https://image.tmdb.org/t/p/w500/"+data.posterPath)
                    .apply(
                        RequestOptions.placeholderOf(com.example.home.R.drawable.ic_loading)
                            .error(com.example.home.R.drawable.ic_error)
                    )
                    .into(imgImage)

                binding.lytContainer.setOnClickListener {
                    val context = binding.root.context
                    val intent = Intent(context, DetailActivity::class.java)
                    intent.putExtra("arg_movie_id",data.id)
                    context.startActivity(intent)
                }
            }
        }
    }
}
