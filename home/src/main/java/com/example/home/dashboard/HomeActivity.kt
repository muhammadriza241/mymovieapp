package com.example.home.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.home.R
import com.example.home.dashboard.fragment.GenreFragment
import com.example.home.dashboard.fragment.HomeFragment
import com.example.home.databinding.ActivityHomeBinding
import com.example.home.module.homeModule
import com.example.home.viewmodel.HomeViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules

class HomeActivity : AppCompatActivity() {
    val binding by lazy { ActivityHomeBinding.inflate(layoutInflater) }

    private val viewModel: HomeViewModel by viewModel()


    var homeFragment = HomeFragment()
    var genreFragment = GenreFragment()
    var active: Fragment = homeFragment

    var ft: FragmentTransaction = supportFragmentManager.beginTransaction()
    val apiKey = "a5956c69becb1fc204b4a2c79a865824"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        loadKoinModules(homeModule)

        initListener()
        binding.lytFilter.visibility = View.INVISIBLE
    }

    private fun initListener() {
        binding.lytBottomHome.setOnClickListener {
            supportFragmentManager.beginTransaction().hide(active).show(homeFragment)
                .commit()
            active = homeFragment
            navigateWithoutAnim(0)
        }
        binding.lytBottomGenre.setOnClickListener {
            supportFragmentManager.beginTransaction().hide(active).show(genreFragment)
                .commit()
            active = genreFragment
            navigateWithoutAnim(1)
        }
    }

    override fun onResume() {
        super.onResume()
        if (!genreFragment.isAdded){
            ft.add(R.id.fragment_container,genreFragment,"2").hide(genreFragment)
        }
        if (!homeFragment.isAdded){
            ft.add(R.id.fragment_container,homeFragment,"1").commit()
        }
    }



    fun navigateWithoutAnim(index: Int) {
        //val controller = Navigation.findNavController(this, R.id.fragment_container)
        when (index) {
            0 -> {
                binding.lytFilter.visibility = View.INVISIBLE
                binding.txtPageTitle.text = resources.getString(com.example.base.R.string.home)
            }
            1 -> {
                binding.lytFilter.visibility = View.VISIBLE
                binding.txtPageTitle.text = resources.getString(com.example.base.R.string.genre)
            }
        }
    }
}