package com.example.home.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.example.core.data.dashboard.response.ResultsItem
import com.example.core.data.dashboard.response.ResultsItemReview
import com.example.core.domain.dashboard.model.GenreListEntity
import com.example.core.domain.dashboard.model.MovieComingSoonEntity
import com.example.core.domain.dashboard.model.MovieDetailEntity
import com.example.core.domain.dashboard.model.MoviePopularEntity
import com.example.core.domain.dashboard.model.MovieReviewEntity
import com.example.core.domain.dashboard.model.MovieTopRatedEntity
import com.example.core.domain.dashboard.model.MovieTrailerEntity
import com.example.core.domain.dashboard.usecase.DashboardUseCase
import com.example.movieapps.core.vo.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class HomeViewModel(private val dashboardUseCase: DashboardUseCase): ViewModel() {

    private val _isLoadingGenreList = MutableLiveData<Boolean>()
    private val _dataGenreList = MutableLiveData<List<GenreListEntity?>?>()
    private val _isErrorGenreList = MutableLiveData<String>()

    private val _isLoadingMoviePopular = MutableLiveData<Boolean>()
    private val _dataMoviePopular = MutableLiveData<List<MoviePopularEntity?>?>()
    private val _isErrorMoviePopular = MutableLiveData<String>()

    private val _isLoadingMovieComingSoon = MutableLiveData<Boolean>()
    private val _dataMovieComingSoon = MutableLiveData<List<MovieComingSoonEntity?>?>()
    private val _isErrorMovieComingSoon = MutableLiveData<String>()

    private val _isLoadingMovieTopRated = MutableLiveData<Boolean>()
    private val _dataMovieTopRated = MutableLiveData<List<MovieTopRatedEntity?>?>()
    private val _isErrorMovieTopRated = MutableLiveData<String>()

    private val _isLoadingMovieDetail = MutableLiveData<Boolean>()
    private val _dataMovieDetail = MutableLiveData<MovieDetailEntity?>()
    private val _isErrorMovieDetail = MutableLiveData<String>()

    private val _isLoadingMovieReview = MutableLiveData<Boolean>()
    private val _dataMovieReview = MutableLiveData<List<MovieReviewEntity?>?>()
    private val _isErrorMovieReview = MutableLiveData<String>()

    private val _isLoadingMovieTrailer = MutableLiveData<Boolean>()
    private val _dataMovieTrailer = MutableLiveData<List<MovieTrailerEntity?>?>()
    private val _isErrorMovieTrailer = MutableLiveData<String>()


    val isLoadingMovieTrailer = _isLoadingMovieTrailer
    val dataMovieTrailer = _dataMovieTrailer
    val isErrorMovieTrailer = _isErrorMovieTrailer

    val isLoadingMovieReview = _isLoadingMovieReview
    val dataMovieReview = _dataMovieReview
    val isErrorMovieReview = _isErrorMovieReview

    val isLoadingMovieDetail = _isLoadingMovieDetail
    val dataMovieDetail = _dataMovieDetail
    val isErrorMovieDetail = _isErrorMovieDetail

    val isLoadingGenreList = _isLoadingGenreList
    val dataGenreList = _dataGenreList
    val isErrorGenreList = _isErrorGenreList

    val isLoadingMoviePopular = _isLoadingMoviePopular
    val dataMoviePopular = _dataMoviePopular
    val isErrorMoviePopular = _isErrorMoviePopular

    val isLoadingMovieComingSoon = _isLoadingMovieComingSoon
    val dataMovieComingSoon = _dataMovieComingSoon
    val isErrorMovieComingSoon = _isErrorMovieComingSoon

    val isLoadingMovieTopRated = _isLoadingMovieTopRated
    val dataMovieTopRated = _dataMovieTopRated
    val isErrorMovieTopRated = _isErrorMovieTopRated

    fun getGenreList(apiKey: String) {
        viewModelScope.launch {
            dashboardUseCase.getGenreList(apiKey)
                .onStart {
                    _isLoadingGenreList.postValue(true)
                }
                .onCompletion {
                    _isLoadingGenreList.postValue(false)
                }
                .collect { data ->
                    when (data) {
                        is Resource.Loading ->
                            _isLoadingGenreList.postValue(true)

                        is Resource.Success -> {
                            _isLoadingGenreList.postValue(false)
                            _dataGenreList.postValue(data.data)
                        }

                        is Resource.Error -> {
                            _isLoadingGenreList.postValue(false)
                            _isErrorGenreList.postValue(data.message!!)
                        }
                    }
                }
        }
    }

    fun getMoviePopular(apiKey: String) {
        viewModelScope.launch {
            dashboardUseCase.getMoviePopular(apiKey)
                .onStart {
                    _isLoadingMoviePopular.postValue(true)
                }
                .onCompletion {
                    _isLoadingMoviePopular.postValue(false)
                }
                .collect { data ->
                    when (data) {
                        is Resource.Loading ->
                            _isLoadingMoviePopular.postValue(true)

                        is Resource.Success -> {
                            _isLoadingMoviePopular.postValue(false)
                            _dataMoviePopular.postValue(data.data)
                        }

                        is Resource.Error -> {
                            _isLoadingMoviePopular.postValue(false)
                            _isErrorMoviePopular.postValue(data.message!!)
                        }
                    }
                }
        }
    }

    fun getMovieComingSoon(apiKey: String) {
        viewModelScope.launch {
            dashboardUseCase.getMovieComingSoon(apiKey)
                .onStart {
                    _isLoadingMovieComingSoon.postValue(true)
                }
                .onCompletion {
                    _isLoadingMovieComingSoon.postValue(false)
                }
                .collect { data ->
                    when (data) {
                        is Resource.Loading ->
                            _isLoadingMovieComingSoon.postValue(true)

                        is Resource.Success -> {
                            _isLoadingMovieComingSoon.postValue(false)
                            _dataMovieComingSoon.postValue(data.data)
                        }

                        is Resource.Error -> {
                            _isLoadingMovieComingSoon.postValue(false)
                            _isErrorMovieComingSoon.postValue(data.message!!)
                        }
                    }
                }
        }
    }

    fun getMovieTopRated(apiKey: String) {
        viewModelScope.launch {
            dashboardUseCase.getMovieTopRated(apiKey)
                .onStart {
                    _isLoadingMovieTopRated.postValue(true)
                }
                .onCompletion {
                    _isLoadingMovieTopRated.postValue(false)
                }
                .collect { data ->
                    when (data) {
                        is Resource.Loading ->
                            _isLoadingMovieTopRated.postValue(true)

                        is Resource.Success -> {
                            _isLoadingMovieTopRated.postValue(false)
                            _dataMovieTopRated.postValue(data.data)
                        }

                        is Resource.Error -> {
                            _isLoadingMovieTopRated.postValue(false)
                            _isErrorMovieTopRated.postValue(data.message!!)
                        }
                    }
                }
        }
    }

    fun getMovieDetail(movieId: String, apiKey: String) {
        viewModelScope.launch {
            dashboardUseCase.getMovieDetail(movieId, apiKey)
                .onStart {
                    _isLoadingMovieDetail.postValue(true)
                }
                .onCompletion {
                    _isLoadingMovieDetail.postValue(false)
                }
                .collect { data ->
                    when (data) {
                        is Resource.Loading ->
                            _isLoadingMovieDetail.postValue(true)

                        is Resource.Success -> {
                            _isLoadingMovieDetail.postValue(false)
                            _dataMovieDetail.postValue(data.data)
                        }

                        is Resource.Error -> {
                            _isLoadingMovieDetail.postValue(false)
                            _isErrorMovieDetail.postValue(data.message!!)
                        }
                    }
                }
        }
    }

    fun getMovieReview(movieId: String, apiKey: String) {
        viewModelScope.launch {
            dashboardUseCase.getMovieReview(movieId, apiKey)
                .onStart {
                    _isLoadingMovieReview.postValue(true)
                }
                .onCompletion {
                    _isLoadingMovieReview.postValue(false)
                }
                .collect { data ->
                    when (data) {
                        is Resource.Loading ->
                            _isLoadingMovieReview.postValue(true)

                        is Resource.Success -> {
                            _isLoadingMovieReview.postValue(false)
                            _dataMovieReview.postValue(data.data)
                        }

                        is Resource.Error -> {
                            _isLoadingMovieReview.postValue(false)
                            _isErrorMovieReview.postValue(data.message!!)
                        }
                    }
                }
        }
    }

    fun getMovieTrailer(movieId: String, apiKey: String) {
        viewModelScope.launch {
            dashboardUseCase.getMovieTrailer(movieId, apiKey)
                .onStart {
                    _isLoadingMovieTrailer.postValue(true)
                }
                .onCompletion {
                    _isLoadingMovieTrailer.postValue(false)
                }
                .collect { data ->
                    when (data) {
                        is Resource.Loading ->
                            _isLoadingMovieTrailer.postValue(true)

                        is Resource.Success -> {
                            _isLoadingMovieTrailer.postValue(false)
                            _dataMovieTrailer.postValue(data.data)
                        }

                        is Resource.Error -> {
                            _isLoadingMovieTrailer.postValue(false)
                            _isErrorMovieTrailer.postValue(data.message!!)
                        }
                    }
                }
        }
    }


    fun getMovieReviewPaging(
        movieId: String,
        apiKey: String
    ): Flow<PagingData<UiReviewModel.DetailListReview>> {
        return dashboardUseCase.getMovieReviewPaging(movieId, apiKey)
            .map { pagingData -> pagingData.map { UiReviewModel.DetailListReview(it) } }
            .cachedIn(viewModelScope)
    }

    fun getMovieListPaging(
        movieId: String,
        apiKey: String
    ): Flow<PagingData<UiMovieModel.ListMovie>> {
        return dashboardUseCase.getMovieListPaging(movieId, apiKey)
            .map { pagingData -> pagingData.map { UiMovieModel.ListMovie(it) } }
            .cachedIn(viewModelScope)
    }

    sealed class UiReviewModel {
        data class DetailListReview(val dataListReview: ResultsItemReview) : UiReviewModel()
    }

    sealed class UiMovieModel {
        data class ListMovie(val dataListMovie: ResultsItem) : UiMovieModel()
    }

}
