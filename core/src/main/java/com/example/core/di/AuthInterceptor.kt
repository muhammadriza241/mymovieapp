package com.example.core.di

import androidx.paging.ExperimentalPagingApi
import com.example.core.util.Utils
import okhttp3.Interceptor
import okhttp3.Response

@ExperimentalPagingApi
class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val apiKey = ""

        val requestBuilder = originalRequest.newBuilder()
            .header("saepay-client-api-key", apiKey)
            .build()

        return chain.proceed(requestBuilder)
    }
}