package com.example.core.di

import androidx.paging.ExperimentalPagingApi
import com.example.core.data.DashboardRepository
import com.example.core.data.dashboard.DashboardRemoteDataSource
import com.example.core.domain.dashboard.repository.IDashboardRepository
import org.koin.dsl.module


@ExperimentalPagingApi
val repositoryModule = module {
    single { DashboardRemoteDataSource(dashboardService = get()) }
    single<IDashboardRepository> { DashboardRepository(dashboardRemoteDataSource = get()) }
}