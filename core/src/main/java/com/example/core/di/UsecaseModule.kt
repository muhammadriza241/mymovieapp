package com.example.core.di

import com.example.core.domain.dashboard.usecase.DashboardInteractor
import com.example.core.domain.dashboard.usecase.DashboardUseCase
import org.koin.dsl.module


val usecaseModule = module {
    factory<DashboardUseCase> { DashboardInteractor(get()) }
}