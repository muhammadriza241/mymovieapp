package com.example.core.domain.dashboard.usecase

import androidx.paging.PagingData
import com.example.core.data.dashboard.response.ResultsItem
import com.example.core.data.dashboard.response.ResultsItemReview
import com.example.core.domain.dashboard.model.GenreListEntity
import com.example.core.domain.dashboard.model.MovieComingSoonEntity
import com.example.core.domain.dashboard.model.MovieDetailEntity
import com.example.core.domain.dashboard.model.MoviePopularEntity
import com.example.core.domain.dashboard.model.MovieReviewEntity
import com.example.core.domain.dashboard.model.MovieTopRatedEntity
import com.example.core.domain.dashboard.model.MovieTrailerEntity
import com.example.movieapps.core.vo.Resource
import kotlinx.coroutines.flow.Flow

interface DashboardUseCase {
    fun getGenreList(apiKey:String): Flow<Resource<List<GenreListEntity>>>
    fun getMoviePopular(apiKey: String): Flow<Resource<List<MoviePopularEntity>>>
    fun getMovieComingSoon(apiKey: String): Flow<Resource<List<MovieComingSoonEntity>>>
    fun getMovieTopRated(apiKey: String): Flow<Resource<List<MovieTopRatedEntity>>>
    fun getMovieDetail(movieId:String,apiKey: String): Flow<Resource<MovieDetailEntity>>
    fun getMovieReview(movieId:String,apiKey: String): Flow<Resource<List<MovieReviewEntity>>>
    fun getMovieReviewPaging(movieId:String,apiKey: String): Flow<PagingData<ResultsItemReview>>
    fun getMovieListPaging(apiKey:String,genre:String): Flow<PagingData<ResultsItem>>
    fun getMovieTrailer(movieId:String,apiKey: String): Flow<Resource<List<MovieTrailerEntity>>>
}