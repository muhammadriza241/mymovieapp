package com.example.core.domain.dashboard.usecase

import androidx.paging.PagingData
import com.example.core.data.dashboard.response.ResultsItem
import com.example.core.data.dashboard.response.ResultsItemReview
import com.example.core.domain.dashboard.model.GenreListEntity
import com.example.core.domain.dashboard.model.MovieComingSoonEntity
import com.example.core.domain.dashboard.model.MovieDetailEntity
import com.example.core.domain.dashboard.model.MoviePopularEntity
import com.example.core.domain.dashboard.model.MovieReviewEntity
import com.example.core.domain.dashboard.model.MovieTopRatedEntity
import com.example.core.domain.dashboard.model.MovieTrailerEntity
import com.example.core.domain.dashboard.repository.IDashboardRepository
import com.example.movieapps.core.vo.Resource
import kotlinx.coroutines.flow.Flow


class DashboardInteractor(private val mainRepository: IDashboardRepository): DashboardUseCase {

    override fun getGenreList(apiKey: String): Flow<Resource<List<GenreListEntity>>> {
        return mainRepository.getGenreList(apiKey)
    }

    override fun getMoviePopular(apiKey: String): Flow<Resource<List<MoviePopularEntity>>> {
        return mainRepository.getMoviePopular(apiKey)
    }

    override fun getMovieComingSoon(apiKey: String): Flow<Resource<List<MovieComingSoonEntity>>> {
        return mainRepository.getMovieComingSoon(apiKey)
    }

    override fun getMovieTopRated(apiKey: String): Flow<Resource<List<MovieTopRatedEntity>>> {
        return mainRepository.getMovieTopRated(apiKey)
    }

    override fun getMovieDetail(
        movieId: String,
        apiKey: String
    ): Flow<Resource<MovieDetailEntity>> {
        return mainRepository.getMovieDetail(movieId, apiKey)
    }

    override fun getMovieReview(
        movieId: String,
        apiKey: String
    ): Flow<Resource<List<MovieReviewEntity>>> {
        return mainRepository.getMovieReview(movieId,apiKey)
    }

    override fun getMovieReviewPaging(
        movieId: String,
        apiKey: String
    ): Flow<PagingData<ResultsItemReview>> {
        return mainRepository.getMovieReviewPaging(movieId, apiKey)
    }

    override fun getMovieListPaging(apiKey: String, genre: String): Flow<PagingData<ResultsItem>> {
        return mainRepository.getMovieListPaging(apiKey, genre)
    }

    override fun getMovieTrailer(
        movieId: String,
        apiKey: String
    ): Flow<Resource<List<MovieTrailerEntity>>> {
        return mainRepository.getMovieTrailer(movieId,apiKey)
    }

}