package com.example.core.domain.dashboard.model

class MovieDetailEntity (
    var id:Int? = null,
    var title: String? = null,
    var posterPath: String? = null,
    var backdropPath: String? = null,
    var overView:String? = null,
    var vote:String? = null,
    var genre:String? = null
)