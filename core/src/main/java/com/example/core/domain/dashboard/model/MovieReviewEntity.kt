package com.example.core.domain.dashboard.model

class MovieReviewEntity (
    var userName : String? = null,
    var avatarPath:String? = null,
    var content:String? = null,
    var rating:Float? = 0.0f
)