package com.example.core.domain.dashboard.model

class MovieComingSoonEntity (
    var id:Int? = null,
    var title: String? = null,
    val posterPath: String? = null,
    val backdropPath: String? = null,
)