package com.example.core.domain.dashboard.model

class MoviePopularEntity(
    var id:Int? = null,
    var title: String? = null,
    var posterPath: String? = null,
    var backdropPath: String? = null,
)