package com.example.core.domain.dashboard.model

class GenreListEntity (
    var name:String?=null,
    var id:Int? = null
)