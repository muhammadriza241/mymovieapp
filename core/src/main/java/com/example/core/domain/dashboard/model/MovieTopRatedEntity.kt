package com.example.core.domain.dashboard.model

class MovieTopRatedEntity (
    var id:Int? = null,
    var title: String? = null,
    val posterPath: String? = null,
    val backdropPath: String? = null,
)