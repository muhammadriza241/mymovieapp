/*
 * Created by Muhamad Riza
 * , 1/8/2023
 * Copyright (c) 2023 by Gibox Digital Asia.
 * All Rights Reserve
 */

package com.example.core.vo

sealed class ApiResponse<out R> {
    data class Success<out T>(val data: T) : ApiResponse<T>()
    data class Error(val errorMessage: String) : ApiResponse<Nothing>()
    object Empty : ApiResponse<Nothing>()
}