package com.example.core.data.dashboard

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.core.data.dashboard.network.DashboardService
import com.example.core.data.dashboard.response.ResponseDetailMovie
import com.example.core.data.dashboard.response.ResponseGenreList
import com.example.core.data.dashboard.response.ResponseListMovie
import com.example.core.data.dashboard.response.ResponseMovieTrailer
import com.example.core.data.dashboard.response.ResponseReviewMovie
import com.example.core.data.dashboard.response.ResultsItem
import com.example.core.data.dashboard.response.ResultsItemReview
import com.example.core.vo.ApiResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.HttpException


@ExperimentalPagingApi
class DashboardRemoteDataSource(private val dashboardService: DashboardService) {


    companion object {
        const val NETWORK_PAGE_SIZE = 10
    }

    suspend fun getGenreList(apiKey:String): Flow<ApiResponse<ResponseGenreList>> {
        return flow {
            try {
                val response = dashboardService.getGenreList(apiKey)
                emit(ApiResponse.Success(response))
            }catch (e: HttpException){
                if (e.code() in 400..499){
                    emit(ApiResponse.Error(e.code().toString()))
                }else if (e.code()==500){
                    emit(ApiResponse.Error(e.code().toString()))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e:Exception){
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getMoviePopular(apiKey:String): Flow<ApiResponse<ResponseListMovie>> {
        return flow {
            try {
                val response = dashboardService.getMoviePopular(apiKey)
                emit(ApiResponse.Success(response))
            }catch (e: HttpException){
                if (e.code() in 400..499){
                    emit(ApiResponse.Error(e.code().toString()))
                }else if (e.code()==500){
                    emit(ApiResponse.Error(e.code().toString()))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e:Exception){
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getMovieComingSoon(apiKey:String): Flow<ApiResponse<ResponseListMovie>> {
        return flow {
            try {
                val response = dashboardService.getMovieComingSoon(apiKey)
                emit(ApiResponse.Success(response))
            }catch (e: HttpException){
                if (e.code() in 400..499){
                    emit(ApiResponse.Error(e.code().toString()))
                }else if (e.code()==500){
                    emit(ApiResponse.Error(e.code().toString()))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e:Exception){
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getMovieTopRated(apiKey:String): Flow<ApiResponse<ResponseListMovie>> {
        return flow {
            try {
                val response = dashboardService.getMovieTopRated(apiKey)
                emit(ApiResponse.Success(response))
            }catch (e: HttpException){
                if (e.code() in 400..499){
                    emit(ApiResponse.Error(e.code().toString()))
                }else if (e.code()==500){
                    emit(ApiResponse.Error(e.code().toString()))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e:Exception){
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getMovieDetail(movieId:String,apiKey:String): Flow<ApiResponse<ResponseDetailMovie>> {
        return flow {
            try {
                val response = dashboardService.getMovieDetail(movieId, apiKey)
                emit(ApiResponse.Success(response))
            }catch (e: HttpException){
                if (e.code() in 400..499){
                    emit(ApiResponse.Error(e.code().toString()))
                }else if (e.code()==500){
                    emit(ApiResponse.Error(e.code().toString()))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e:Exception){
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getMovieReview(movieId:String,apiKey:String): Flow<ApiResponse<ResponseReviewMovie>> {
        return flow {
            try {
                val response = dashboardService.getMovieReview(movieId, apiKey)
                emit(ApiResponse.Success(response))
            }catch (e: HttpException){
                if (e.code() in 400..499){
                    emit(ApiResponse.Error(e.code().toString()))
                }else if (e.code()==500){
                    emit(ApiResponse.Error(e.code().toString()))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e:Exception){
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }
    suspend fun getMovieTrailer(movieId:String,apiKey:String): Flow<ApiResponse<ResponseMovieTrailer>> {
        return flow {
            try {
                val response = dashboardService.getMovieTrailer(movieId, apiKey)
                emit(ApiResponse.Success(response))
            }catch (e: HttpException){
                if (e.code() in 400..499){
                    emit(ApiResponse.Error(e.code().toString()))
                }else if (e.code()==500){
                    emit(ApiResponse.Error(e.code().toString()))
                }else{
                    emit(ApiResponse.Empty)
                }
            }catch (e:Exception){
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }

    fun getMovieReviewPaging(movieId:String,apiKey:String): Flow<PagingData<ResultsItemReview>> {
        return Pager(
            config = PagingConfig(pageSize = NETWORK_PAGE_SIZE,enablePlaceholders = false),
            pagingSourceFactory = {ReviewListPagingSource(dashboardService,movieId, apiKey)}
        ).flow
    }

    fun getMovieListPaging(apiKey:String,genre:String): Flow<PagingData<ResultsItem>> {
        return Pager(
            config = PagingConfig(pageSize = NETWORK_PAGE_SIZE,enablePlaceholders = false),
            pagingSourceFactory = {MovieListPagingSource(dashboardService,apiKey,genre)}
        ).flow
    }


}