package com.example.core.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import com.example.core.data.dashboard.DashboardRemoteDataSource
import com.example.core.domain.dashboard.model.GenreListEntity
import com.example.core.domain.dashboard.model.MoviePopularEntity
import com.example.core.domain.dashboard.model.*
import com.example.core.domain.dashboard.repository.IDashboardRepository
import com.example.core.persistence.mapper.dashboard.DashboardDataMapper
import com.example.core.vo.ApiResponse
import com.example.movieapps.core.vo.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import com.example.core.data.dashboard.response.ResultsItemReview
import com.example.core.data.dashboard.response.ResultsItem

@ExperimentalPagingApi
class DashboardRepository(private val dashboardRemoteDataSource: DashboardRemoteDataSource):IDashboardRepository {

    override fun getGenreList(apiKey: String): Flow<Resource<List<GenreListEntity>>> = flow {
        emit(Resource.Loading())
        when(val apiResponse = dashboardRemoteDataSource.getGenreList(apiKey).first()){
            is ApiResponse.Success->{
                val response = DashboardDataMapper.mapResponseGenreListToEntity(apiResponse.data.genres)
                emit(Resource.Success(response))
            }
            is ApiResponse.Empty->{
                emit(Resource.Error("Error"))
            }
            is ApiResponse.Error->{
                emit(Resource.Error(apiResponse.errorMessage))
            }
        }
    }

    override fun getMoviePopular(apiKey: String): Flow<Resource<List<MoviePopularEntity>>> = flow {
        emit(Resource.Loading())
        when(val apiResponse = dashboardRemoteDataSource.getMoviePopular(apiKey).first()){
            is ApiResponse.Success->{
                val response = DashboardDataMapper.mapResponseMoviePopularToEntity(apiResponse.data.results)
                emit(Resource.Success(response))
            }
            is ApiResponse.Empty->{
                emit(Resource.Error("Error"))
            }
            is ApiResponse.Error->{
                emit(Resource.Error(apiResponse.errorMessage))
            }
        }
    }

    override fun getMovieComingSoon(apiKey: String): Flow<Resource<List<MovieComingSoonEntity>>> = flow {
        emit(Resource.Loading())
        when(val apiResponse = dashboardRemoteDataSource.getMovieComingSoon(apiKey).first()){
            is ApiResponse.Success->{
                val response = DashboardDataMapper.mapResponseMovieComingSoonToEntity(apiResponse.data.results)
                emit(Resource.Success(response))
            }
            is ApiResponse.Empty->{
                emit(Resource.Error("Error"))
            }
            is ApiResponse.Error->{
                emit(Resource.Error(apiResponse.errorMessage))
            }
        }
    }

    override fun getMovieTopRated(apiKey: String): Flow<Resource<List<MovieTopRatedEntity>>> = flow {
        emit(Resource.Loading())
        when(val apiResponse = dashboardRemoteDataSource.getMovieTopRated(apiKey).first()){
            is ApiResponse.Success->{
                val response = DashboardDataMapper.mapResponseMovieTopRatedToEntity(apiResponse.data.results)
                emit(Resource.Success(response))
            }
            is ApiResponse.Empty->{
                emit(Resource.Error("Error"))
            }
            is ApiResponse.Error->{
                emit(Resource.Error(apiResponse.errorMessage))
            }
        }
    }

    override fun getMovieDetail(
        movieId: String,
        apiKey: String
    ): Flow<Resource<MovieDetailEntity>> = flow {
        emit(Resource.Loading())
        when(val apiResponse = dashboardRemoteDataSource.getMovieDetail(movieId,apiKey).first()){
            is ApiResponse.Success->{
                val response = DashboardDataMapper.mapResponseMovieDetailToEntity(apiResponse.data)
                emit(Resource.Success(response))
            }
            is ApiResponse.Empty->{
                emit(Resource.Error("Error"))
            }
            is ApiResponse.Error->{
                emit(Resource.Error(apiResponse.errorMessage))
            }
        }
    }

    override fun getMovieReview(
        movieId: String,
        apiKey: String
    ): Flow<Resource<List<MovieReviewEntity>>> = flow {
        emit(Resource.Loading())
        when(val apiResponse = dashboardRemoteDataSource.getMovieReview(movieId,apiKey).first()){
            is ApiResponse.Success->{
                val response = DashboardDataMapper.mapResponseMovieReviewToEntity(apiResponse.data.results)
                emit(Resource.Success(response))
            }
            is ApiResponse.Empty->{
                emit(Resource.Error("Error"))
            }
            is ApiResponse.Error->{
                emit(Resource.Error(apiResponse.errorMessage))
            }
        }
    }

    override fun getMovieReviewPaging(
        movieId: String,
        apiKey: String
    ): Flow<PagingData<ResultsItemReview>> {
        return dashboardRemoteDataSource.getMovieReviewPaging(movieId, apiKey)
    }

    override fun getMovieListPaging(apiKey: String, genre: String): Flow<PagingData<ResultsItem>> {
        return dashboardRemoteDataSource.getMovieListPaging(apiKey, genre)
    }

    override fun getMovieTrailer(
        movieId: String,
        apiKey: String
    ): Flow<Resource<List<MovieTrailerEntity>>> = flow {
        emit(Resource.Loading())
        when(val apiResponse = dashboardRemoteDataSource.getMovieTrailer(movieId,apiKey).first()){
            is ApiResponse.Success->{
                val response = DashboardDataMapper.mapResponseMovieTrailerToEntity(apiResponse.data.results)
                emit(Resource.Success(response))
            }
            is ApiResponse.Empty->{
                emit(Resource.Error("Error"))
            }
            is ApiResponse.Error->{
                emit(Resource.Error(apiResponse.errorMessage))
            }
        }
    }

}