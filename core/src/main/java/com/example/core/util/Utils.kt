package com.example.core.util

import com.example.core.data.dashboard.response.GenresItem
import com.example.core.domain.dashboard.model.GenreListEntity

object Utils {

    fun genreListToString(listGenre:List<GenresItem?>):String{
        var result = ""
        for (item in listGenre){
            if (result==""){
                result = "${item?.name}"
            }else{
                result = "$result,${item?.name}"
            }
        }
        return result
    }
    fun genreListToString(listGenre:ArrayList<GenreListEntity?>):String{
        var result = ""
        for (item in listGenre){
            if (result==""){
                result = "${item?.id}"
            }else{
                result = "$result,${item?.id}"
            }
        }
        return result
    }


    private external fun baseUrl() : String

    fun getBaseURL() : String = baseUrl()
    private external fun apikey() : String
    fun getApiKey() : String = apikey()

}